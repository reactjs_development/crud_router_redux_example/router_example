import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Login from '../login/login';
import Logout from '../logout/logout';
import Register from '../register/register';
import NotFound from '../notFound/notFound';
import Categories from '../categories/categories';
import NavBar from '../navigation/navBar';
function Routers() {
 

  return (

    <BrowserRouter>
    <NavBar/>
      <Routes >
        <Route path="/" element={<Login />} />
        <Route path="logout" element={<Logout />} />
        <Route path="register" element={<Register />} />
        <Route path="categories/:categroyId"  element={ <Categories home={"true"}/>}  />
        <Route path="categories" element={<Categories />} />
.
        <Route path="*" element={<NotFound />} />

      </Routes>
    </BrowserRouter>

  );
}

export default Routers;
