import React from "react";
import {NavLink} from 'react-router-dom';
import './navBar.css'

export default function NavBar(){
    
return (
<div className="NavBar">
<NavLink  to='/'>Login</NavLink>
<NavLink to='/logout'>Logout</NavLink>
<NavLink to='/categories'>Categories</NavLink>
<NavLink to='/register'>Register</NavLink>

</div>
);

}