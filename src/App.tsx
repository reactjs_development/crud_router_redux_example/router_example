import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routers from './components/routers/routers';
function App() {
  return (
    <div className="App">
      <Routers />
    </div>
  );
}

export default App;
